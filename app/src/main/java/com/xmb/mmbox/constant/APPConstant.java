package com.xmb.mmbox.constant;

/**
 * 所有常量写在这里
 * Created by dengz on 2017/11/6.
 */
public class APPConstant {

    //视频MP4播放地址的默认服务器地址：
    public static final String URL_DEF_VIDEO_IP = "113.16.215.11";

    //在线APP参数缓存在本地的时间（秒）
    public static final int APP_CONFIG_CACHE_TIME = 60 * 60 * 24;//秒

}
