package com.xmb.mmbox;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.squareup.picasso.Picasso;
import com.xmb.mmbox.service.AppConfigLoader;
import com.xmb.mmbox.service.VideoLoader;
import com.xmb.mmbox.video.ViPPlayerView;
import com.xmb.mmbox.vo.AppOnlineConfig;
import com.xmb.mmbox.vo.VideoVO;

import java.util.ArrayList;
import java.util.Random;

import cn.jzvd.JZVideoPlayer;

public class VideoListFragment extends BaseMmboxFragment {
    private static final int COUNT_LOAD_PER_TIME = 10;//每次加载的数量
    Context mContext;
    ListView listView;
    private AppOnlineConfig mConfig;
    private RefreshLayout refreshLayout;
    private VideoListAdapter adapter;
    private ArrayList<VideoVO> listAll;//所有的
    private ArrayList<VideoVO> listShow;//显示的
    private LayoutInflater inflater;
//    private AppUser mAppUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        VideoListFragment.this.inflater = inflater;
        View v = inflater.inflate(R.layout.vd_video_list_ui, container, false);
        mContext = getContext();
        initData(v);
        return v;
    }

    public void initData(View v) {
//        mAppUser = AppUserManager.loadAppUser(getContext());
        listView = v.findViewById(R.id.listview);
        adapter = new VideoListAdapter();
        listView.setAdapter(adapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                JZVideoPlayer.onScrollReleaseAllVideos(view, firstVisibleItem, visibleItemCount,
                        totalItemCount);
            }
        });
        refreshLayout = (RefreshLayout) v.findViewById(R.id.refreshLayout);
        new RefreshThread().start();
        buildRefresh();
    }

    private void buildRefresh() {
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//下拉刷新
                new RefreshThread().start();
            }
        });
        refreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {//上拉加载更多
                new LoadMoreThread().start();
            }
        });
    }

    /**
     * 做第一次加载数据 或 下拉刷新的处理
     */
    class RefreshThread extends Thread {
        @Override
        public void run() {
            if (mConfig != null) {//有本地数据，就故意加一些延时，像是在线加载一样
                try {
                    Thread.sleep(randomDelayTime());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mConfig = AppConfigLoader.loadSmart(getActivity());//先加载全局参数
            if (listShow != null) listShow.clear();
            listAll = VideoLoader.loadShuffleVideoList();
            limitList2Show();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshLayout.finishRefresh();
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    class LoadMoreThread extends Thread {
        @Override
        public void run() {
            if (mConfig != null) {//有本地数据，就故意加一些延时，像是在线加载一样
                try {
                    Thread.sleep(randomDelayTime());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            limitList2Show();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshLayout.finishLoadmore();
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    private long randomDelayTime() {
        return new Random().nextInt(1200) + 800;
    }

    private void limitList2Show() {
        if (listShow == null) listShow = new ArrayList<VideoVO>();
        for (int i = 0; i < COUNT_LOAD_PER_TIME; i++) {
            if (listAll.size() <= 0)
                return;
            listShow.add(listAll.remove(0));
        }
    }

    private ViewHolder hold;

    static class ViewHolder {
        ViPPlayerView vipPlayer;
    }

    class VideoListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            if (listShow == null) {
                return 0;
            }
            return listShow.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (null == convertView) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.vd_video_play, null);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.vipPlayer = convertView.findViewById(R.id.videoplayerViP);
            viewHolder.vipPlayer.setVisibility(View.VISIBLE);

            VideoVO vo = listShow.get(position);
            if (vo != null) {
                String title = vo.getTitle();
                String mp4Url = vo.buildVideoURL(mConfig, vo);
                String picUrl = vo.buildVideoCoverURL(mConfig, vo);

                viewHolder.vipPlayer.setUp(
                        mp4Url, JZVideoPlayer.SCREEN_WINDOW_LIST,
                        title);
                Picasso.with(convertView.getContext())
                        .load(picUrl)
                        .into(viewHolder.vipPlayer.thumbImageView);
                viewHolder.vipPlayer.positionInList = position;
            }
            return convertView;
        }
    }
}