package com.xmb.mmbox;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

public class MMBox {

    @NonNull
    public static Fragment getPicFragment() {
        return new PicListFragment();
    }

    @NonNull
    public static Fragment getVideoFragment() {
        return new VideoListFragment();
    }

    public static void startPicActivity(Activity act, String title, boolean isShowBack) {
        PicListActivity.start(act, title, isShowBack);
    }

    public static void startVideoActivity(Activity act, String title, boolean isShowBack) {
        VideoListActivity.start(act, title, isShowBack);
    }

}
