package com.xmb.mmbox.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * http://blog.csdn.net/nnmmbb/article/details/28419779
 * Created by dengz on 2017/11/3.
 */

public class ViewPagerFixed extends android.support.v4.view.ViewPager {
    public ViewPagerFixed(Context context) {
        super(context);
    }

    public ViewPagerFixed(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            return super.onTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
