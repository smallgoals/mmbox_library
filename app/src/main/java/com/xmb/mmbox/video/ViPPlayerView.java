package com.xmb.mmbox.video;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.xmb.mmbox.R;

import java.util.LinkedHashMap;

import cn.jzvd.JZVideoPlayerStandard;

/**
 * 这里可以监听到视频播放的生命周期和播放状态
 * 所有关于视频的逻辑都应该写在这里
 * Created by Nathen on 2017/7/2.
 */
public class ViPPlayerView extends JZVideoPlayerStandard {
    public ViPPlayerView(Context context) {
        super(context);
    }
    public ViPPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setUp(LinkedHashMap urlMap, int defaultUrlMapIndex, int screen, Object... objects) {
        super.setUp(urlMap, defaultUrlMapIndex, screen, objects);
        // 隐藏视频标题：
        try {
            if (titleTextView == null) {
                titleTextView = findViewById(R.id.title);
            }
            titleTextView.setVisibility(View.INVISIBLE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
