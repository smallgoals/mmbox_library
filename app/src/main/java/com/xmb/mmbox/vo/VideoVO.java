package com.xmb.mmbox.vo;

import com.xmb.mmbox.constant.APPConstant;

import java.io.Serializable;
import java.util.Random;
import java.util.zip.CRC32;

/**
 * Created by Administrator on 11/3/2017.
 */
public class VideoVO implements Serializable {
    private String title;
    private String fileName;
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
    public void setFileName(String fileName){
        this.fileName = fileName;
    }
    public String getFileName(){
        return this.fileName;
    }
    @Override
    public String toString() {
        return "VideoVO [title=" + title + ", fileName=" + fileName + "]";
    }

    /**
     * 获取视频MP4地址的URL
     */
    public static String buildVideoURL(AppOnlineConfig config, VideoVO vo) {
        String vdUrl = config.getVideo_mp4_url();
        vdUrl = vdUrl.replace("${fileName}", vo.fileName);
        String[] vdIPs = config.getVideo_mp4_ip();
        String ip = APPConstant.URL_DEF_VIDEO_IP;
        if (vdIPs != null && vdIPs.length > 0) {
            int len = vdIPs.length;
            ip = vdIPs[new Random().nextInt(len)%len];
        }
        vdUrl = vdUrl.replace("${video_mp4_ip}", ip);
        return vdUrl;
    }

    /**
     * 获取视频封面图片URL
     */
    public static String buildVideoCoverURL(AppOnlineConfig config, VideoVO vo) {
        String vdUrl = config.getVideo_cover_url();
        vdUrl = vdUrl.replace("${fileName}", vo.getFileName());
        return vdUrl;
    }

    public static int getMillisecond(String fileName) {
        CRC32 crc32 = new CRC32();
        crc32.update(fileName.getBytes());
        int time = (int)(crc32.getValue() & 0x6ddd00);
        if(time < 30*60*1000){
            time += 35*60*1000;
        }
        return  time;
    }
    public static String castTime(String _time) {
        int time = getMillisecond(_time);
        int hour = 0;
        int minute = 0;
        int second = 0;

        second = time / 1000;
        if (second > 60) {
            minute = second / 60;
            second = second % 60;
        }
        if (minute > 60) {
            hour = minute / 60;
            minute = minute % 60;
        }
        return String.format("%02d:%02d:%02d",hour,minute,second);
    }
}
