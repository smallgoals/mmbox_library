package com.xmb.mmbox.vo;

import java.io.Serializable;

/**
 * Created by dengz on 2017/11/1.
 */
public class PicVo implements Serializable {

    private String title;
    private String fileName;
    private String url;
    private int size;

    @Override
    public String toString() {
        return "PicVo [title=" + title + ", fileName=" + fileName + ", url="
                + url + ", size=" + size + "]";
    }


    /**
     * 获取指定页码的图片URL
     *
     * @param config
     * @param vo
     * @param page
     * @return
     */
    public static String buildPicURLByPage(AppOnlineConfig config, PicVo vo, int page) {
        return buildPicURLByPage(config, vo.getFileName(), page);
    }

    public static String buildPicURLByPage(AppOnlineConfig config, String fileName, int page) {
        String picUrl = config.getPic_url();
        picUrl = picUrl.replace("${fileName}", fileName);
        picUrl = picUrl.replace("${page}", page + "");
        return picUrl;
    }

    /**
     * 获取封面图片URL
     *
     * @param config
     * @param vo
     * @return
     */
    public static String buildCoverURL(AppOnlineConfig config, PicVo vo) {
        String picUrl = config.getPic_url();
        picUrl = picUrl.replace("${fileName}", vo.getFileName());
        String page = "9";
        if (vo.getSize() < 9) {
            page = "1";
        }
        picUrl = picUrl.replace("${page}", page);
        return picUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
