package com.xmb.mmbox.vo;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 在线参数
 * Created by dengz on 2017/11/1.
 */

public class AppOnlineConfig implements Serializable {
    private String vip1_price;
    private String vip2_price;
    private String vip3_price;
    private String pic_url;
    private String video_cover_url;
    private String video_mp4_url;
    private String[] video_mp4_ip;

    @Override
    public String toString() {
        return "AppOnlineConfig{" +
                "vip1_price='" + vip1_price + '\'' +
                ", vip2_price='" + vip2_price + '\'' +
                ", vip3_price='" + vip3_price + '\'' +
                ", pic_url='" + pic_url + '\'' +
                ", video_cover_url='" + video_cover_url + '\'' +
                ", video_mp4_url='" + video_mp4_url + '\'' +
                ", video_mp4_ip=" + Arrays.toString(video_mp4_ip) +
                '}';
    }

    public String[] getVideo_mp4_ip() {
        return video_mp4_ip;
    }

    public void setVideo_mp4_ip(String[] video_mp4_ip) {
        this.video_mp4_ip = video_mp4_ip;
    }

    public String getVip1_price() {
        return vip1_price;
    }

    public void setVip1_price(String vip1_price) {
        this.vip1_price = vip1_price;
    }

    public String getVip2_price() {
        return vip2_price;
    }

    public void setVip2_price(String vip2_price) {
        this.vip2_price = vip2_price;
    }

    public String getVip3_price() {
        return vip3_price;
    }

    public void setVip3_price(String vip3_price) {
        this.vip3_price = vip3_price;
    }

    public String getPic_url() {
        return pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }

    public String getVideo_cover_url() {
        return video_cover_url;
    }

    public void setVideo_cover_url(String video_cover_url) {
        this.video_cover_url = video_cover_url;
    }

    public String getVideo_mp4_url() {
        return video_mp4_url;
    }

    public void setVideo_mp4_url(String video_mp4_url) {
        this.video_mp4_url = video_mp4_url;
    }
}
