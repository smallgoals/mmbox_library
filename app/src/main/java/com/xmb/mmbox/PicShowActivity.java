package com.xmb.mmbox;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.imagepipeline.image.ImageInfo;
import com.xmb.mmbox.util.MyDownloader;
import com.xmb.mmbox.vo.AppOnlineConfig;
import com.xmb.mmbox.vo.PicVo;

import java.io.IOException;
import java.util.ArrayList;

import me.relex.photodraweeview.OnViewTapListener;
import me.relex.photodraweeview.PhotoDraweeView;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * 图片展示
 */
public class PicShowActivity extends BaseMmboxActivity implements ViewPager
        .OnPageChangeListener {
    private ViewPager viewpager;
    private ArrayList<View> viewList;
    private PicVo picVo;
    private AppOnlineConfig config;
    private TextView tv_cur_page;
    private TextView tv_title;
    //    private AppUser appUser;
    //    private ImageView iv_vip;
    //用户点击了图片才显示的菜单：
    private ViewGroup layout_menu;
    //当前菜单是否处于显示：
    private boolean isVisible;
    private int curIndex;

    //权限
    private static final int PERMISION_SD_CARD = 1;

    public static void start(Activity act, AppOnlineConfig config, PicVo picVo) {
        Intent it = new Intent(act, PicShowActivity.class);
        it.putExtra("config", config);
        it.putExtra("picVo", picVo);
        act.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 禁止截屏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        //去除标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //去除状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        picVo = (PicVo) getIntent().getSerializableExtra("picVo");
        config = (AppOnlineConfig) getIntent().getSerializableExtra("config");
        setContentView(R.layout.activity_pic_show);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        layout_menu = (ViewGroup) findViewById(R.id.layout_menu);
        tv_cur_page = (TextView) findViewById(R.id.tv_cur_page);
        tv_title = (TextView) findViewById(R.id.tv_title);
//        iv_vip = (ImageView) findViewById(R.id.iv_vip);
        initView();
    }

    private void initView() {
//        appUser = AppUserManager.loadAppUser(this);
        //标题
        tv_title.setText(picVo.getTitle());
        //页码
        String curPage = 1 + "/" + picVo.getSize();
        tv_cur_page.setText(curPage);

        viewpager = (ViewPager) findViewById(R.id.viewpager); // 获取viewpager
        viewList = new ArrayList<View>(); // 保存view，用于PagerAdapter
        LayoutInflater inflate = LayoutInflater.from(this);
        for (int i = 1; i <= picVo.getSize(); i++) {
            View rootView = inflate.inflate(R.layout.imageview_pic2, null);
            final PhotoDraweeView iv = (PhotoDraweeView) rootView.findViewById(R.id
                    .photo_drawee_view);
            //用户点击图片-> 显示菜单
            iv.setOnViewTapListener(new OnViewTapListener() {
                @Override
                public void onViewTap(View view, float x, float y) {
                    isVisible = !isVisible;
                    if (isVisible) {
                        layout_menu.setVisibility(View.VISIBLE);
                    } else {
                        layout_menu.setVisibility(View.GONE);
                    }
                }
            });

            String urlStr = PicVo.buildPicURLByPage(config, picVo, i);
//            if (i > APPConstant.FREE_USER_SHOW_MAX_PIC && !appUser.isVIP1()) {
            //做高斯模糊，开通会员才能看高清的图片：
//                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(urlStr))
//                        .setPostprocessor(new IterativeBoxBlurPostProcessor(2, 15 + i))
//                        .build();
//                AbstractDraweeController controller = Fresco.newDraweeControllerBuilder()
//                        .setOldController(iv.getController())
//                        .setImageRequest(request)
//                        .build();
//                iv.setController(controller);
//            } else {//正常图片
//                iv.setImageURI(urlStr);
            PipelineDraweeControllerBuilder controller = Fresco.newDraweeControllerBuilder();
            controller.setUri(Uri.parse(urlStr));
            controller.setOldController(iv.getController());
            controller.setControllerListener(new BaseControllerListener<ImageInfo>() {
                @Override
                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable
                        animatable) {
                    super.onFinalImageSet(id, imageInfo, animatable);
                    if (imageInfo == null || iv == null) {
                        return;
                    }
                    iv.update(imageInfo.getWidth(), imageInfo.getHeight());
                }
            });
            iv.setController(controller.build());

//            }
            viewList.add(iv);
        }
        viewpager.setAdapter(pagerAdapter); // 加入适配器
        viewpager.addOnPageChangeListener(this);
        viewpager.setCurrentItem(0);
    }


    /**
     * ViewPager的适配器 重写下面几个方法就可以了
     */

    PagerAdapter pagerAdapter = new PagerAdapter() {

        @Override
        public int getCount() {
            return viewList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(viewList.get(position));

        }

        @Override
        public int getItemPosition(Object object) {

            return super.getItemPosition(object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(viewList.get(position));
            return viewList.get(position);
        }

    };

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        curIndex = position;
        //更新页码
        String curPage = (position + 1) + "/" + picVo.getSize();
        tv_cur_page.setText(curPage);
//        if (position >= APPConstant.FREE_USER_SHOW_MAX_PIC && !appUser.isVIP1()) {
//            iv_vip.setVisibility(View.VISIBLE);
//            Animation animation = AnimationUtils.loadAnimation(this, R.anim.scale_vip);
//            animation.setRepeatCount(Animation.INFINITE);
//            iv_vip.startAnimation(animation);
//        } else {
//            iv_vip.clearAnimation();
//            iv_vip.setVisibility(View.GONE);
//        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * 点击了返回
     */
    public void onClickBack(View view) {
        finish();
    }


    public void onClickFavorite(View view) {
//        if (curIndex >= APPConstant.FREE_USER_SHOW_MAX_PIC && !appUser.isVIP1()) return;
//        boolean isSuc = FavoritesManager.add(this, new FavoritesVo(picVo.getFileName(), curIndex
//                + 1));
        Toast.makeText(this, "标星成功，以后为您推荐相关口味类型的美图", Toast.LENGTH_SHORT).show();
    }

    /**
     * 点击了“升级VIP”
     */
//    public void onClickVIP(View view) {
//        showVipTips();
//    }

    /**
     * 点击了下载
     */
    public void onClickDownload(View view) {
//        if (!appUser.isVIP1()) {//还不是图片会员，则提醒开通：
//            showVipTips();
//        } else {
        downloadRequiresPermission();
//        }
    }

//    private void doPay() {
//        new MainPay(PicShowActivity.this, new MainPay.PayListener() {
//            @Override
//            public void onPayResult(int payResultState, PayResult
//                    payResult) {
//                if (payResultState == MainPay.PAY_RESULT_SUC) {
//                    Toast.makeText(PicShowActivity.this, "恭喜，升级成功！", Toast
//                            .LENGTH_LONG).show();
//                    appUser.setVIP1(true);
//                    AppUserManager.saveAppUser(PicShowActivity.this, appUser);
//                    finish();
//                } else {
//                    Toast.makeText(PicShowActivity.this, "支付失败，请重试", Toast
//                            .LENGTH_LONG).show();
//                }
//            }
//        }).pay("图片黄金VIP", "图片黄金会员" + ManifestsUtil.getPayBody(this), config.getVip1_price());
//    }

//    private void showPicVipTips() {
//        String msg = "1.海量套图无限看(后面超精彩)\n2.超清HD大图下载\n3.每天更新套图\n\n永久有效,仅需" + config.getVip1_price()
//                + "元";
//        new AlertDialog.Builder(this).setTitle("升级到图片黄金会员").setMessage(msg)
//                .setPositiveButton
//                        ("立即升级", new
//                                DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int
//                                            i) {
//                                        doPay();
//                                    }
//                                }).setNegativeButton("继续平庸", null).setCancelable(false).show();
//    }

    @AfterPermissionGranted(PERMISION_SD_CARD)
    private void downloadRequiresPermission() {
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            String urlStr = PicVo.buildPicURLByPage(config, picVo, curIndex + 1);
//            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File
//                    .separator + "mmbox";
            try {
                MyDownloader.download(this, urlStr, System.currentTimeMillis() + ".jpg");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(this, "已经添加到后台下载，稍后请查看相册", Toast.LENGTH_LONG).show();
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "保存图片需要文件读取权限，请允许",
                    PERMISION_SD_CARD, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

}
