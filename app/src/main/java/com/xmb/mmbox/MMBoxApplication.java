package com.xmb.mmbox;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;


public class MMBoxApplication extends Application /*CrashApp*/ {
    public static Context context;

    public static void init(Context context) {
        MMBoxApplication.context = context;
        Fresco.initialize(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = MMBoxApplication.this.getApplicationContext();
        Fresco.initialize(context);
    }
}
