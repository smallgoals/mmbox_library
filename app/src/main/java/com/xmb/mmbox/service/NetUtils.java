package com.xmb.mmbox.service;

import android.content.Context;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by dengz on 2017/11/1.
 */

public class NetUtils {


    public static String getFromAssets(Context context, String assetsFileFullName) {
        try {
            //Return an AssetManager instance for your application's package
            InputStream is = context.getAssets().open(assetsFileFullName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String text = new String(buffer, "utf-8");
            return text;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 用GET方法请求HTTP
     *
     * @param urlStr
     * @return 可能为Null
     */
    public static String get(String urlStr) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(6000);
            conn.setReadTimeout(6000);
            conn.connect();
            int code = conn.getResponseCode();
            if (code == 200) {
                InputStream is = conn.getInputStream();
                String state = getStreamFromInputstream(is);
                return state;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    //根据输入流返回字符串
    private static String getStreamFromInputstream(InputStream is) throws Exception {
        String result = "";
        ByteArrayOutputStream baos;
        baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = -1;
        while ((len = is.read(buffer)) != -1) {
            baos.write(buffer, 0, len);
        }
        is.close();
        byte[] lens = baos.toByteArray();
        result = new String(lens);//
        baos.close();
        return result;
    }


}
