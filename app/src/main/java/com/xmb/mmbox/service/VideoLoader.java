package com.xmb.mmbox.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xmb.mmbox.MMBoxApplication;
import com.xmb.mmbox.vo.VideoVO;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 从服务器上加载视频信息
 * Created by Yinql on 2017/11/5.
 */
public class VideoLoader {
    public static ArrayList<VideoVO> loadShuffleVideoList() {
        ArrayList<VideoVO> list = loadVideoList();
        Collections.shuffle(list);
        return list;
    }

    public static ArrayList<VideoVO> loadVideoList() {
        ArrayList<VideoVO> list = new ArrayList<>();
        try {
//            String result = NetUtils.get(APPConstant.URL_VIDEO_LIST);
            String result = NetUtils.getFromAssets(MMBoxApplication.context, "video_list" +
                    ".json");
            Log.i("VideoLoader", result);
            ArrayList<VideoVO> rs = new ArrayList<VideoVO>();
            Type type = new TypeToken<ArrayList<VideoVO>>() {
            }.getType();
            list = new Gson().fromJson(result, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


}
