package com.xmb.mmbox.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xmb.mmbox.MMBoxApplication;
import com.xmb.mmbox.vo.PicVo;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 从服务器上加载图片信息
 * Created by dengz on 2017/11/1.
 */

public class PicLoader {

    public static ArrayList<PicVo> loadShufflePicList() {
        ArrayList<PicVo> list = loadPicList();
        Collections.shuffle(list);
        return list;
    }

    /**
     * @return
     */
    public static ArrayList<PicVo> loadPicList() {
        ArrayList<PicVo> list = new ArrayList<>();
        try {
//            String result = NetUtils.get(APPConstant.URL_PIC_LIST);
            String result = NetUtils.getFromAssets(MMBoxApplication.context, "pic_list" +
                    ".json");
            Log.i("PicLoader", result);
            ArrayList<PicVo> rs = new ArrayList<PicVo>();
            Type type = new TypeToken<ArrayList<PicVo>>() {
            }.getType();
            list = new Gson().fromJson(result, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


}
