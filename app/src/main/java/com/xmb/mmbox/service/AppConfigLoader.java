package com.xmb.mmbox.service;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.xmb.mmbox.MMBoxApplication;
import com.xmb.mmbox.constant.APPConstant;
import com.xmb.mmbox.util.ACache;
import com.xmb.mmbox.vo.AppOnlineConfig;


/**
 * 在线参数
 * Created by dengz on 2017/11/1.
 */
public class AppConfigLoader {

    /**
     * 智能获取：先从本地缓存读，如果没有缓存或者缓存过时了，再从网络获取
     *
     * @param ctx
     * @return
     */
    public static AppOnlineConfig loadSmart(Context ctx) {
        AppOnlineConfig conf = loadFromCache(ctx);
        if (conf == null) {
            conf = loadFromWeb();
            Log.d("AppConfigLoader", "AppConfigLoader 在线加载");
            ACache a = ACache.get(ctx);
            a.put("AppOnlineConfig", conf, APPConstant.APP_CONFIG_CACHE_TIME);
        } else {
            Log.d("AppConfigLoader", "AppConfigLoader 本地缓存");
        }
        return conf;
    }

    /**
     * 获取缓存在本地的设置
     *
     * @param ctx
     * @return 可能会为NULL
     */
    public static AppOnlineConfig loadFromCache(Context ctx) {
        ACache a = ACache.get(ctx);
        AppOnlineConfig conf = (AppOnlineConfig) a.getAsObject("AppOnlineConfig");
        return conf;
    }


    public static AppOnlineConfig loadFromWeb() {
        AppOnlineConfig vo = new AppOnlineConfig();
        try {
//            String result = NetUtils.get(APPConstant.URL_APP_CONFIG);
            String result = NetUtils.getFromAssets(MMBoxApplication.context, "app_config" +
                    ".json");
            Log.i("AppConfigLoader", result);
            vo = new Gson().fromJson(result, AppOnlineConfig.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vo;
    }


}
