package com.xmb.mmbox;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xmb.mmbox.service.AppConfigLoader;
import com.xmb.mmbox.service.PicLoader;
import com.xmb.mmbox.vo.AppOnlineConfig;
import com.xmb.mmbox.vo.PicVo;

import java.util.ArrayList;
import java.util.Random;


/**
 * 图片列表
 */
public class PicListActivity extends BaseAppCompatActivity implements AdapterView.OnItemClickListener {
    private static final int COUNT_LOAD_PER_TIME = 10;//每次加载的数量

    private AppOnlineConfig config;
    private RefreshLayout refreshLayout;
    private ListView lv_pic;
    private PicListAdapter adapter;
    private ArrayList<PicVo> listAll;//所有的
    private ArrayList<PicVo> listShow;//显示的
    private LayoutInflater inflater;


    public static void start(Activity act, String title, boolean isShowBack) {
        Intent it = new Intent(act, PicListActivity.class);
        it.putExtra("title", title);
        it.putExtra("isShowBack", isShowBack);
        act.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pic_list);

        String title = getIntent().getStringExtra("title");
        boolean isShowBack = getIntent().getBooleanExtra("isShowBack", true);

        setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isShowBack);

        inflater = LayoutInflater.from(this);
        adapter = new PicListAdapter();
        lv_pic = findViewById(R.id.lv_pic);
        refreshLayout = (RefreshLayout) findViewById(R.id.refreshLayout);
        lv_pic.setAdapter(adapter);
        lv_pic.setOnItemClickListener(this);
        new RefreshThread().start();
        buildRefresh();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:   //返回键的id
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void buildRefresh() {
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {//下拉刷新
                new RefreshThread().start();
            }
        });

        refreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {//上拉加载更多
                new LoadMoreThread().start();
            }
        });
    }

    public Activity getActivity() {
        return this;
    }

    /**
     * 做第一次加载数据 或 下拉刷新的处理
     */
    class RefreshThread extends Thread {
        @Override
        public void run() {
            if (config != null) {//有本地数据，就故意加一些延时，像是在线加载一样
                try {
                    Thread.sleep(randomDelayTime());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            config = AppConfigLoader.loadSmart(getActivity());//先加载全局参数
            if (listShow != null) listShow.clear();
            listAll =
                    PicLoader.loadShufflePicList();
            limitList2Show();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshLayout.finishRefresh();
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    ;

    class LoadMoreThread extends Thread {
        @Override
        public void run() {
            if (config != null) {//有本地数据，就故意加一些延时，像是在线加载一样
                try {
                    Thread.sleep(randomDelayTime());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            limitList2Show();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshLayout.finishLoadmore();
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    ;


    private long randomDelayTime() {
        return new Random().nextInt(1200) + 800;
    }

    private void limitList2Show() {
        if (listShow == null) listShow = new ArrayList<PicVo>();
        for (int i = 0; i < COUNT_LOAD_PER_TIME; i++) {
            if (listAll.size() <= 0)
                return;
            listShow.add(listAll.remove(0));
        }
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        PicShowActivity.start(getActivity(), config, listShow.get(i));
    }

    private ViewHold hold;

    static class ViewHold {
        SimpleDraweeView iv_pic;
        TextView tv_title, tv_size;

    }

    class PicListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            if (listShow == null) {
                return 0;
            }
            return listShow.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_listview_pic, null);
                hold = new ViewHold();
                hold.iv_pic = (SimpleDraweeView) convertView.findViewById(R.id.iv_pic);
                hold.tv_size = (TextView) convertView
                        .findViewById(R.id.tv_size);
                hold.tv_title = (TextView) convertView
                        .findViewById(R.id.tv_title);
                convertView.setTag(hold);
            } else {
                hold = (ViewHold) convertView.getTag();
            }

            PicVo vo = listShow.get(position);
            hold.tv_size.setText(vo.getSize() + "P");
            hold.tv_title.setText(vo.getTitle());
            try {
                Uri uri = Uri.parse(PicVo.buildCoverURL(config, vo));
                hold.iv_pic.setImageURI(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }
    }

}
