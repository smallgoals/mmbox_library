package com.xmb.mmbox;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.ashokvarma.bottomnavigation.ShapeBadgeItem;

import cn.jzvd.JZVideoPlayer;

public class MainActivity extends BaseMmboxActivity {
    private Fragment curFrag;
    private Fragment frag_pics, frag_videos;
    private BottomNavigationBar bottomNavigationBar;
    private ShapeBadgeItem badge;//红点点

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottom_navigation_bar);
        buildBadge();
        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.index_icon_video_unsel, "爽片"))
                .addItem(new BottomNavigationItem(R.drawable.index_icon_photo_unsel, "美图"))
                .initialise();
        bottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position) {
                changeFrag(position);
            }

            @Override
            public void onTabUnselected(int position) {
            }

            @Override
            public void onTabReselected(int position) {
            }
        });

        selectTab(0);
    }

    public void selectTab(int index) {
        bottomNavigationBar.selectTab(index);
    }

    /**
     * 加个红点点
     */
    private void buildBadge() {
        badge = new ShapeBadgeItem().setShape(ShapeBadgeItem.SHAPE_OVAL)
                .setSizeInDp(this, 8, 8).setHideOnSelect(true);
    }

    private void changeFrag(int position) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // 先隐藏掉所有的Fragment，以防止有多个Fragment显示在界面上的情况
        if (curFrag != null) {
            transaction.hide(curFrag);
        }
        switch (position) {
            case 0:
                if (frag_videos == null) {
                    frag_videos = new VideoListFragment();
                    transaction.add(R.id.frag_container, frag_videos);
                } else {
                    transaction.show(frag_videos);
                }
                curFrag = frag_videos;
                break;
            case 1:
                if (frag_pics == null) {
                    frag_pics = new PicListFragment();
                    transaction.add(R.id.frag_container, frag_pics);
                } else {
                    transaction.show(frag_pics);
                }
                curFrag = frag_pics;
                break;

        }

        //消除动画效果
        disableABCShowHideAnimation(getSupportActionBar());
        getSupportActionBar().show();
        transaction.commitAllowingStateLoss();
    }

    public void onBackPressed() {
        // 按下返回键时，若视频正在播放中先退出视频播放
        if (JZVideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    protected void onPause() {
        super.onPause();
        // 释放已播放的全部视频资源
        JZVideoPlayer.releaseAllVideos();
    }


}
