package com.xmb.mmbox.util;

import android.os.Environment;

import com.arialyy.aria.core.Aria;

import java.io.File;
import java.io.IOException;

/**
 * Created by dengz on 2017/11/6.
 */

public class MyDownloader {

    public static String getMyPath() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                .getAbsolutePath();
    }

    public static void download(Object obj, String url, String fileName) throws IOException {
        download(obj, url, getMyPath(), fileName);
    }

    /**
     * @param obj      obj 支持类型有【Activity、Service、Application、DialogFragment、Fragment
     *                 、PopupWindow、Dialog】
     * @param url
     * @param filePath
     */
    public static void download(Object obj, String url, String filePath, String fileName) throws
            IOException {
        File dir = new File(filePath);
        boolean suc = dir.mkdirs();
        File file = new File(dir.getAbsolutePath() + File.separatorChar + fileName);
        file.createNewFile();
        Aria.download(obj)
                .load(url)     //读取下载地址
                .setDownloadPath(file.getAbsolutePath()) //设置文件保存的完整路径
                .start();   //启动下载
    }


}
