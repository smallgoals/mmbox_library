package com.mmbox.sample;

import android.jiakao.handsgo.com.sample.R;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.xmb.mmbox.MMBox;

public class MainActivity extends AppCompatActivity {
    private ViewGroup container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = findViewById(R.id.container);
    }

    public void onClickShowPicByFrag(View view) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, MMBox.getPicFragment());
        transaction.commitAllowingStateLoss();
    }

    public void onClickShowVideoByFrag(View view) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, MMBox.getVideoFragment());
        transaction.commitAllowingStateLoss();
    }

    public void onClickShowPicByAct(View view) {
        MMBox.startPicActivity(this, "自定义标题", false);
    }

    public void onClickShowVideoByAct(View view) {
        MMBox.startVideoActivity(this, "自定义标题", true);
    }
}
